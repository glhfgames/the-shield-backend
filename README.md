# README #

This README documents the steps that are necessary to get the application up and running.

### What is this repository for? ###

* This repository is for the backend of 'The Shield' game.
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies - Java/LibGDX
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* If you have any enquiries please contact Tristyn Mackay at tristynmackay@hotmail.com
* Other community or team contact