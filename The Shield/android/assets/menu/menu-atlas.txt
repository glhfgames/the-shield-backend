menu-atlas.png
size: 21, 90
format: RGBA8888
filter: Linear,Linear
repeat: none
Scroll_Thumb
  rotate: false
  xy: 0, 12
  size: 15, 15
  orig: 15, 15
  offset: 0, 0
  index: -1
Slider_Horizontal
  rotate: false
  xy: 0, 0
  size: 10, 12
  split: 5, 5, 0, 0
  orig: 10, 12
  offset: 0, 0
  index: -1
Slider_Vertical
  rotate: false
  xy: 10, 0
  size: 11, 10
  split: 0, 0, 5, 5
  orig: 11, 10
  offset: 0, 0
  index: -1
TextBox
  rotate: false
  xy: 0, 48
  size: 21, 21
  split: 7, 7, 14, 14
  orig: 21, 21
  offset: 0, 0
  index: -1
TextBox_Dark
  rotate: false
  xy: 0, 27
  size: 21, 21
  split: 7, 7, 14, 14
  orig: 21, 21
  offset: 0, 0
  index: -1
TextBox_Light
  rotate: false
  xy: 0, 69
  size: 21, 21
  split: 7, 7, 14, 14
  orig: 21, 21
  offset: 0, 0
  index: -1
