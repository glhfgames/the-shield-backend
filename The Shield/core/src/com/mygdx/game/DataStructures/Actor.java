package com.mygdx.game.DataStructures;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Floors.Floor;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class Actor extends GameObject {

    public Vector2 Position;
    public Enums.Direction Direction;

    public ActorType type;

    public TextureRegion[] sprites;
    public int currentSpriteIndex = 0;
    public Enums.SpriteDirection spriteDirection;

    public enum ActorType {
        NONE,
        WALL,
        FLOOR,
        TRAP,
        BUFF,
        PLAYER,
        ENEMY,
        PROJECTILE,
        EFFECT
    }


    // Setters.
    public void setPosition(Vector2 position) {
        Position = position;
    }

    public void setDirection(Enums.Direction direction) {
        Direction = direction;
    }

    public void setSpriteDirection(Enums.SpriteDirection direction) {
        spriteDirection = direction;
    }
}
