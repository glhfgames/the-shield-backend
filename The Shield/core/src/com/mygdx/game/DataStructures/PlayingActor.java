package com.mygdx.game.DataStructures;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class PlayingActor extends Actor {

    // Variables.
    public int lives = 1;
    public boolean isDead = false;

    public double movementSpeed = 0.5;
    public boolean isMoving = false;
    public double rotationSpeed = 0.3;
    public boolean isRotatingLeft = false;
    public boolean isRotatingRight = false;

    public Enums.AttackType attackMode = Enums.AttackType.MELEE;
    public double attackSpeed = 0.5;
    public boolean isAttacking = false;
    public double blockSpeed = 0.3;
    public boolean isBlocking = false;
    public boolean isSwappingWeapons = false;


    // Methods.

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the movement methods. (Make sure to implement the camera methods first).
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Movement.
    public void MoveForward() {
        // Smoothly move the player forward 1 unit (square).
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the rotation methods. (Make sure to implement the camera methods first).
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Rotation.
    public void TurnLeft() {
        if (performingActions() == false) {
            // Play the rotate character Left sprite.
            // Change the characters direction
            // (i.e. if the character's current direction is NORTH, change their Direction to WEST).
        }
    }

    public void TurnRight() {
        if (performingActions() == false) {
            // Play the rotate character Right sprite.
            // Change the characters direction
            // (i.e. if the character's current direction is NORTH, change their Direction to EAST).
        }
    }


    // Combat.
    public void MeleeAttack() {
        if (attackMode == Enums.AttackType.MELEE && performingActions() == false) {
            // The player can attack.
        }
        else {
            // Display to the player that they can't perform an attack right now.
        }
    }

    public void RangedAttack() {
        if (attackMode == Enums.AttackType.RANGED && performingActions() == false) {
            // The player can attack.
        }
        else {
            // Display to the player that they can't perform an attack right now.
        }
    }

    public void Block() {
        if (attackMode == Enums.AttackType.MELEE && performingActions() == false) {
            // The player can attack.
        }
        else {
            // Display to the player that they can't perform a block right now.
        }
    }


    // Checks.
    public boolean performingActions() {
        if (isAttacking == true || isBlocking == true ||
                isMoving == true || isRotatingLeft == true ||
                isRotatingRight == true || isSwappingWeapons == true) {
            return true;
        }
        return false;
    }
}
