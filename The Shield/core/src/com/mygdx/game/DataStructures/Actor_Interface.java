package com.mygdx.game.DataStructures;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Solist on 3/04/2017.
 */

public interface Actor_Interface {
    TextureRegion getSprite();

    void render();
}
