package com.mygdx.game.DataStructures;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class GameObject {

    public GameController gameController;


    // Setters.
    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }
}
