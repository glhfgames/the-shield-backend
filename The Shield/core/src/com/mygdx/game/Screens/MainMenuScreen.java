package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public class MainMenuScreen extends GameScreen implements Screen {

    public MainMenuScreen(GameController gameController) {
        this.gameController = gameController;

        skin = gameController.assetManager.menuSkin;

        sprites = gameController.assetManager.menuSprites;

        gameController.assetManager.menuMusic = Gdx.audio.newMusic(Gdx.files.internal("sound/music/TheShield.mp3"));
    }

    @Override
    public void show() {
        gameController.assetManager.setStage(gameController.assetManager.menuSprites);

        // Create a table.
        Table mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the table's contents.
        mainTable.top();

        // Create the body table.
        Table bodyTable = new Table();

        // Create the screen's title.
        Label mainMenuTitle = new Label("The Shield", skin, "label_Large");

        // Create the screen's buttons.
        TextButton newGameButton = new TextButton("New Game", skin, "textButton");
        TextButton loadGameButton = new TextButton("Load Game", skin, "textButton");
        TextButton optionsButton = new TextButton("Options", skin, "textButton");
        TextButton helpButton = new TextButton("Help", skin, "textButton");
        TextButton creditsButton = new TextButton("Credits", skin, "textButton");
        TextButton exitButton = new TextButton("Exit", skin, "textButton");


        newGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.newGameScreen);
            }
        });

        loadGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.loadGameScreen);
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.optionsScreen);
            }
        });

        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.helpScreen);
            }
        });

        creditsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.creditsScreen);
            }
        });

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                Gdx.app.exit();
            }
        });

        // Add the buttons to the body table.
        bodyTable.add(newGameButton).width(100);
        bodyTable.row();
        bodyTable.add(loadGameButton).width(100);
        bodyTable.row();
        bodyTable.add(optionsButton).width(100);
        bodyTable.row();
        bodyTable.add(helpButton).width(100);
        bodyTable.row();
        bodyTable.add(creditsButton).width(100);
        bodyTable.row();
        bodyTable.add(exitButton).width(100);
        bodyTable.row();

        // Add the contents to the main table.
        mainTable.add(mainMenuTitle);
        mainTable.row();
        mainTable.add(bodyTable).expand();

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);

        if (gameController.assetManager.menuMusic.isPlaying() == false) {
            gameController.assetManager.menuMusic.setVolume(1);
            gameController.assetManager.menuMusic.play();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Music loop
        if (gameController.assetManager.menuMusic.getPosition() >= 32.3f) {
            gameController.assetManager.menuMusic.setPosition(8.2f);
        }

        displayBackground(gameController.assetManager.backgroundSprite,
                            gameController.assetManager.backgroundTexture);

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);
        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                                            gameController.uiCamera.viewportHeight / 2, 0);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        gameController.assetManager.menuMusic.stop();
        gameController.dispose();
    }
}
