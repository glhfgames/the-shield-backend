package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public class OptionsScreen extends GameScreen implements Screen {

    private Slider masterVolumeSlider;
    private Label masterVolumePercent;
    private Slider musicVolumeSlider;
    private Label musicVolumePercent;
    private Slider ambienceVolumeSlider;
    private Label ambienceVolumePercent;
    private Slider effectsVolumeSlider;
    private Label effectsVolumePercent;
    private Slider dialogueVolumeSlider;
    private Label dialogueVolumePercent;

    private float previousMasterVolume;
    private float previousMusicVolume;
    private float previousAmbienceVolume;
    private float previousEffectsVolume;
    private float previousDialogueVolume;

    public OptionsScreen(GameController gameController) {
        this.gameController = gameController;

        skin = gameController.assetManager.menuSkin;

        sprites = gameController.assetManager.menuSprites;
    }

    @Override
    public void show() {
        gameController.assetManager.setStage(gameController.assetManager.menuSprites);

        previousMasterVolume = gameController.masterVolume;
        previousMusicVolume = gameController.musicVolume;
        previousAmbienceVolume = gameController.ambienceVolume;
        previousEffectsVolume = gameController.effectsVolume;
        previousDialogueVolume = gameController.dialogueVolume;

        // Create a table.
        Table mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the tables contents.
        mainTable.top();

        // Create the options Table.
        Table optionsTable = new Table();

        // Create the screen's title.
        Label optionsTitle = new Label("Options", skin, "label_Large");

        // Create the options UI items.
        Label volumeLabel = new Label("Volume Options", skin);

        Label masterVolumeLabel = new Label("Master Volume : ", skin);
        masterVolumeSlider = new Slider(0, 1, 0.005f, false, skin);
        if (masterVolumeSlider.getValue() != gameController.masterVolume) {
            masterVolumeSlider.setValue(gameController.masterVolume);
        }
        masterVolumePercent = new Label((masterVolumeSlider.getValue() * 100) + " %", skin);

        Label musicVolumeLabel = new Label("Music Volume : ", skin);
        musicVolumeSlider = new Slider(0, 1, 0.005f, false, skin);
        if (musicVolumeSlider.getValue() != gameController.musicVolume) {
            musicVolumeSlider.setValue(gameController.musicVolume);
        }
        musicVolumePercent = new Label((musicVolumeSlider.getValue() * 100) + " %", skin);

        Label ambienceVolumeLabel = new Label("Ambience Volume : ", skin);
        ambienceVolumeSlider = new Slider(0, 1, 0.005f, false, skin);
        if (ambienceVolumeSlider.getValue() != gameController.ambienceVolume) {
            ambienceVolumeSlider.setValue(gameController.ambienceVolume);
        }
        ambienceVolumePercent = new Label((ambienceVolumeSlider.getValue() * 100) + " %", skin);

        Label effectsVolumeLabel = new Label("Effects Volume : ", skin);
        effectsVolumeSlider = new Slider(0, 1, 0.005f, false, skin);
        if (effectsVolumeSlider.getValue() != gameController.effectsVolume) {
            effectsVolumeSlider.setValue(gameController.effectsVolume);
        }
        effectsVolumePercent = new Label((effectsVolumeSlider.getValue() * 100) + " %", skin);

        Label dialogueVolumeLabel = new Label("Dialogue Volume : ", skin);
        dialogueVolumeSlider = new Slider(0, 1, 0.005f, false, skin);
        if (dialogueVolumeSlider.getValue() != gameController.dialogueVolume) {
            dialogueVolumeSlider.setValue(gameController.dialogueVolume);
        }
        dialogueVolumePercent = new Label((dialogueVolumeSlider.getValue() * 100) + " %", skin);

        // Setup the options table.
        optionsTable.add(volumeLabel).expandX().colspan(5);
        optionsTable.row();
        optionsTable.add().width(20);
        optionsTable.add(masterVolumeLabel).expandX().left();
        optionsTable.add(masterVolumeSlider);
        optionsTable.add(masterVolumePercent).width(80).right();
        optionsTable.add().width(20);
        optionsTable.row();
        optionsTable.add().width(20);
        optionsTable.add(musicVolumeLabel).expandX().left();
        optionsTable.add(musicVolumeSlider);
        optionsTable.add(musicVolumePercent).width(80).right();
        optionsTable.add().width(20);
        optionsTable.row();
        optionsTable.add().width(20);
        optionsTable.add(ambienceVolumeLabel).expandX().left();
        optionsTable.add(ambienceVolumeSlider);
        optionsTable.add(ambienceVolumePercent).width(80).right();
        optionsTable.add().width(20);
        optionsTable.row();
        optionsTable.add().width(20);
        optionsTable.add(effectsVolumeLabel).expandX().left();
        optionsTable.add(effectsVolumeSlider);
        optionsTable.add(effectsVolumePercent).width(80).right();
        optionsTable.add().width(20);
        optionsTable.row();
        optionsTable.add().width(20);
        optionsTable.add(dialogueVolumeLabel).expandX().left();
        optionsTable.add(dialogueVolumeSlider);
        optionsTable.add(dialogueVolumePercent).width(80).right();
        optionsTable.add().width(20);

        ScrollPane scrollPane = new ScrollPane(optionsTable, skin);

        // Create the screen's buttons.
        TextButton applyButton = new TextButton("Apply", skin, "textButton");
        TextButton backButton = new TextButton("Back", skin, "textButton");


        applyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Make the changes.
                // Return to the previous screen.
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameController.masterVolume = previousMasterVolume;
                gameController.musicVolume = previousMusicVolume;
                gameController.ambienceVolume = previousAmbienceVolume;
                gameController.effectsVolume = previousEffectsVolume;
                gameController.dialogueVolume = previousDialogueVolume;
                gameController.updateVolume();
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        // Add the buttons to the tables.
        mainTable.add(optionsTitle).expandX().colspan(5);
        mainTable.row();
        mainTable.add(scrollPane).expand().colspan(5);
        mainTable.row();
        mainTable.add().height(10).expandX().colspan(5);
        mainTable.row();
        mainTable.add().width(100);
        mainTable.add(backButton).width(100).left();
        mainTable.add().expandX();
        mainTable.add(applyButton).width(100).right();
        mainTable.add().width(100);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Update options settings.
        if (masterVolumeSlider.getValue() != gameController.masterVolume) {
            gameController.masterVolume = masterVolumeSlider.getValue();
            masterVolumePercent.setText((masterVolumeSlider.getValue() * 100) + " %");
            gameController.updateVolume();
        }
        if (musicVolumeSlider.getValue() != gameController.musicVolume) {
            gameController.musicVolume = musicVolumeSlider.getValue();
            musicVolumePercent.setText((musicVolumeSlider.getValue() * 100) + " %");
            gameController.updateMusicVolume();
        }
        if (ambienceVolumeSlider.getValue() != gameController.ambienceVolume) {
            gameController.ambienceVolume = ambienceVolumeSlider.getValue();
            ambienceVolumePercent.setText((ambienceVolumeSlider.getValue() * 100) + " %");
            gameController.updateAmbienceVolume();
        }
        if (effectsVolumeSlider.getValue() != gameController.effectsVolume) {
            gameController.effectsVolume = effectsVolumeSlider.getValue();
            effectsVolumePercent.setText((effectsVolumeSlider.getValue() * 100) + " %");
            gameController.updateEffectsVolume();
        }
        if (dialogueVolumeSlider.getValue() != gameController.dialogueVolume) {
            gameController.dialogueVolume = dialogueVolumeSlider.getValue();
            dialogueVolumePercent.setText((dialogueVolumeSlider.getValue() * 100) + " %");
            gameController.updateDialogueVolume();
        }

        // Music loop
        if (gameController.assetManager.menuMusic.getPosition() >= 32.3f) {
            gameController.assetManager.menuMusic.setPosition(8.2f);
        }

        //displayBackground(gameController.assetManager.backgroundSprite,
        //                    gameController.assetManager.backgroundTexture);

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);
        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                                            gameController.uiCamera.viewportHeight / 2, 0);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

