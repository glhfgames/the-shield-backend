package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;

/**
 * Created by Solist on 26/03/2017.
 */

public class InGameScreen extends GameScreen implements Screen {

    // Variables.
    public boolean uiRendered = false;

    TiledMapTileLayer playerLayer;

    private Button menuButton;
    private Button swapSwordButton;
    private Button swapBowButton;
    private Button shieldButton;
    private Button swordButton;
    private Button bowButton;

    private boolean weaponChanged = false;

    private Button compassNorthImage;
    private Button compassEastImage;
    private Button compassSouthImage;
    private Button compassWestImage;

    private Table mainTable;

    private Table menuTable;

    private Skin menuSkin;

    private boolean menuActive = false;
    private boolean menuDisplayed = false;


    public InGameScreen(GameController gameController) {
        this.gameController = gameController;

        menuSkin = gameController.assetManager.menuSkin;

        skin = gameController.assetManager.uiSkin;

        sprites = gameController.assetManager.uiSprites;
    }


    public void show() {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        gameController.inGame = true;

        gameController.inGameCamera.camera = new OrthographicCamera();
        gameController.inGameCamera.camera.setToOrtho(false, w, h);
        gameController.inGameCamera.camera.update();

        playerLayer = (TiledMapTileLayer)gameController.level.map.getLayers().get(6);
        boolean loopFinish = false;
        for (int i = 0; i < playerLayer.getWidth(); i++) {
            for (int k = 0; k < playerLayer.getHeight(); k++) {
                if (playerLayer.getCell(i, k) != null) {
                    gameController.inGameCamera.Position = new Vector2(i, k);
                    int mapHeight = gameController.level.floorLayer.getHeight();
                        gameController.player.Position = new Vector2 (64 + (i * 128),
                                (mapHeight * 128) - (64 + (mapHeight - (k + 1)) * 128));
                    loopFinish = true;
                    break;
                }
                if (loopFinish == true) {
                    break;
                }
            }
        }

        gameController.assetManager.setStage(sprites);

        // Create the menu table.
        menuTable = new Table();
        // Make the table fill the stage.
        menuTable.setFillParent(true);
        // Set the alignment of the table's contents.
        menuTable.top();
        // Create the body table for the menu.
        Table menuBodyTable = new Table();

        // Create the table's title.
        Label menuTitle = new Label("Menu", menuSkin, "label_Large");

        // Create the table's buttons.
        TextButton resumeButton = new TextButton("Resume", menuSkin, "textButton");
        TextButton saveButton = new TextButton("Save", menuSkin, "textButton");
        TextButton loadButton = new TextButton("Load", menuSkin, "textButton");
        TextButton optionsButton = new TextButton("Options", menuSkin, "textButton");
        TextButton helpButton = new TextButton("Help", menuSkin, "textButton");
        TextButton exitButton = new TextButton("Exit", menuSkin, "textButton");


        resumeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameController.inGame = true;
                menuActive = false;
                uiRendered = false;
            }
        });

        saveButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Save Screen.
                uiRendered = false;
            }
        });

        loadButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Load Screen.
                uiRendered = false;
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
            }
        });

        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
            }
        });

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Make the changes.
                uiRendered = false;
                // Return to the previous screen.
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        // Add the buttons to the body table.
        menuBodyTable.add(resumeButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(saveButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(loadButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(optionsButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(helpButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(exitButton).width(100);

        // Add the contents to the main table.
        menuTable.add(menuTitle);
        menuTable.row();
        menuTable.add(menuBodyTable).expand();


        // Create the ui table.
        mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the table's contents.
        mainTable.top();

        // Create the screen's buttons.
        menuButton = new Button(skin, "MenuButton");
        swapSwordButton = new Button(skin, "SwapSwordButton");
        swapBowButton = new Button(skin, "SwapBowButton");
        shieldButton = new Button(skin, "ShieldButton");
        swordButton = new Button(skin, "SwordButton");
        bowButton = new Button(skin, "BowButton");
        // Create the screen's images.
        compassNorthImage = new Button(skin, "CompassNorth");
        compassEastImage = new Button(skin, "CompassEast");
        compassSouthImage = new Button(skin, "CompassSouth");
        compassWestImage = new Button(skin, "CompassWest");

        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameController.inGame = false;
                menuActive = true;
                menuDisplayed = false;
                uiRendered = false;
            }
        });

        swapSwordButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.performingActions() == false) {
                    // Switch weapons.
                    gameController.player.attackMode = Enums.AttackType.MELEE;
                    weaponChanged = true;
                    uiRendered = false;
                }
            }
        });

        swapBowButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.performingActions() == false) {
                    // Switch weapons.
                    gameController.player.attackMode = Enums.AttackType.RANGED;
                    weaponChanged = true;
                    uiRendered = false;
                }
            }
        });

        swordButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.performingActions() == false) {
                    // Attack.
                }
            }
        });

        shieldButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.performingActions() == false) {
                    // Block.
                }
            }
        });

        bowButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.performingActions() == false) {
                    // Ranged Attack.
                }
            }
        });

        populateUITable();

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (gameController.assetManager.menuMusic.isPlaying()) {
            gameController.assetManager.menuMusic.setVolume(gameController.assetManager.menuMusic.getVolume() / 1.4f);
            if (gameController.assetManager.menuMusic.getVolume() < 0.01f) {
                gameController.assetManager.menuMusic.stop();
            }
        }

        gameController.assetManager.inputController.CheckInputs();

        if (gameController.player.isRotatingLeft) {
            gameController.inGameCamera.TurnLeft(delta);
        }
        else if (gameController.player.isRotatingRight) {
            gameController.inGameCamera.TurnRight(delta);
        }
        else if (gameController.player.isMoving) {
            gameController.inGameCamera.MoveForward(delta);
        }

        gameController.inGameCamera.camera.update();
        gameController.level.mapRenderer.setView(gameController.inGameCamera.camera);
        gameController.level.mapRenderer.render();


        // Render the traps and buffs.

        // Render the player/enemies.
        //gameBatch.begin();
        //gameController.player.render();
        //gameBatch.end();

        // Render the projectiles.

        // Render the special effects.

        // Render the UI.
        if (uiRendered == false) {
            if (weaponChanged == true) {
                populateUITable();
            }

            // Render the menu.
            if (menuActive == true) {
                if (menuDisplayed == false) {
                    menuDisplayed = true;
                    populateMenuTable();
                }
            } else {
                if (menuDisplayed == true) {
                    menuDisplayed = false;
                    populateUITable();
                }
            }
            uiRendered = true;
        }

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }


    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);

        gameController.inGameCamera.camera.position.set(gameController.player.Position.x,
                gameController.player.Position.y, 10);
        gameController.inGameCamera.camera.zoom += 1;
        gameController.inGameCamera.camera.update();

        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                    gameController.uiCamera.viewportHeight / 2, 10);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void populateUITable() {
        weaponChanged = false;

        mainTable.clearChildren();
        gameController.assetManager.stage.clear();

        // Add the buttons to the tables.
        mainTable.row();
        mainTable.add().height(20).fillX().colspan(5);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(menuButton).top().left();
        mainTable.add().fillX();
        switch (gameController.inGameCamera.Direction) {
            case NORTH:
                mainTable.add(compassNorthImage).top().right().colspan(2);
                break;
            case EAST:
                mainTable.add(compassEastImage).top().right().colspan(2);
                break;
            case SOUTH:
                mainTable.add(compassSouthImage).top().right().colspan(2);
                break;
            case WEST:
                mainTable.add(compassWestImage).top().right().colspan(2);
                break;
            default:
                break;
        }
        mainTable.add().width(20);
        mainTable.row();
        mainTable.add().fillY();
        mainTable.add().fillY();
        mainTable.add().expand();
        mainTable.add().fillY();
        mainTable.add().fillY();
        mainTable.row();
        mainTable.add().width(20);
        if (gameController.player.attackMode == Enums.AttackType.MELEE) {
            mainTable.add(swapBowButton);
            mainTable.add().fillX();
            mainTable.add(shieldButton);
            mainTable.add(swordButton);
        }
        else {
            mainTable.add(swapSwordButton);
            mainTable.add().fillX();
            mainTable.add().width(48);
            mainTable.add(bowButton);
        }
        mainTable.add().width(20);
        mainTable.row();
        mainTable.add().height(20).fillX().colspan(5);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    private void populateMenuTable() {
        gameController.assetManager.stage.clear();

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(menuTable);
    }
}
