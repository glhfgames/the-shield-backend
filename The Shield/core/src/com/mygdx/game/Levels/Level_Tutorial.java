package com.mygdx.game.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 3/04/2017.
 */

public class Level_Tutorial extends Level {

    public Level_Tutorial (GameController gameController){
        Init("Level_Tutorial", "Floor_Grey", "Wall_Grey");
    }
}
