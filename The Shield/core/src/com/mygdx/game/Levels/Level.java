package com.mygdx.game.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 31/03/2017.
 */

public class Level extends GameObject {

    public TiledMap map;
    public TiledMapTileSet floorTiles;
    public TiledMapTileSet wallTiles;
    public TiledMapTileSet trapAndBuffTiles;
    public TiledMapTileSet playerTiles;
    public TiledMapTileSet projectileTiles;
    public TiledMapTileSet specialEffectTiles;
    public TiledMapTileLayer floorLayer;
    public TiledMapTileLayer wallNorthLayer;
    public TiledMapTileLayer wallEastLayer;
    public TiledMapTileLayer wallSouthLayer;
    public TiledMapTileLayer wallWestLayer;
    public TiledMapTileLayer trapAndBuffLayer;
    public TiledMapTileLayer playerLayer;
    public TiledMapTileLayer projectileLayer;
    public TiledMapTileLayer specialEffectLayer;

    public TiledMapRenderer mapRenderer;

    public int radius = 5;


    public void Init(String tileMapName, String floorTileSetName, String wallTileSetName) {
        String mapPathName = Gdx.files.internal("level/" + tileMapName + ".tmx").path();
        map = new TmxMapLoader().load(mapPathName);

        mapRenderer = new OrthogonalTiledMapRenderer(map);

        MapLayers layers = map.getLayers();

        floorLayer = (TiledMapTileLayer) layers.get("Floors");
        wallNorthLayer = (TiledMapTileLayer) layers.get("Walls_NORTH");
        wallEastLayer = (TiledMapTileLayer) layers.get("Walls_EAST");
        wallSouthLayer = (TiledMapTileLayer) layers.get("Walls_SOUTH");
        wallWestLayer = (TiledMapTileLayer) layers.get("Walls_WEST");
        trapAndBuffLayer = (TiledMapTileLayer) layers.get("TrapsAndBuffs");
        playerLayer = (TiledMapTileLayer) layers.get("Players");
        projectileLayer = (TiledMapTileLayer) layers.get("Projectiles");
        specialEffectLayer = (TiledMapTileLayer) layers.get("SpecialEffects");

        floorTiles = map.getTileSets().getTileSet(floorTileSetName);
        wallTiles = map.getTileSets().getTileSet(wallTileSetName);
        trapAndBuffTiles = map.getTileSets().getTileSet("TrapsAndBuffs");
        playerTiles = map.getTileSets().getTileSet("Players");
        projectileTiles = map.getTileSets().getTileSet("Projectiles");
        specialEffectTiles = map.getTileSets().getTileSet("SpecialEffects");
    }


    public TiledMapTileLayer.Cell getFloorCell(int x, int y) {
        return floorLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getWallCell(int x, int y) {
        if (wallNorthLayer.isVisible()) {
            return wallNorthLayer.getCell(x, y);
        } else if (wallEastLayer.isVisible()) {
            return wallEastLayer.getCell(x, y);
        } else if (wallSouthLayer.isVisible()) {
            return wallSouthLayer.getCell(x, y);
        } else {
            return wallWestLayer.getCell(x, y);
        }
    }


    public TiledMapTileLayer.Cell getTrapOrBuffCell(int x, int y) {
        return trapAndBuffLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getPlayerCell(int x, int y) {
        return playerLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getProjectileCell(int x, int y) {
        return projectileLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getSpecialEffectCell(int x, int y) {
        return specialEffectLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell[] getFloors(Vector2 referencePosition) {
        return getCellsInRange(floorLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getWalls(Vector2 referencePosition) {
        if (wallNorthLayer.isVisible()) {
            return getCellsInRange(wallNorthLayer, referencePosition);
        } else if (wallEastLayer.isVisible()) {
            return getCellsInRange(wallEastLayer, referencePosition);
        } else if (wallSouthLayer.isVisible()) {
            return getCellsInRange(wallSouthLayer, referencePosition);
        } else {
            return getCellsInRange(wallWestLayer, referencePosition);
        }
    }


    public TiledMapTileLayer.Cell[] getTrapsAndBuffs(Vector2 referencePosition) {
        return getCellsInRange(trapAndBuffLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getPlayers(Vector2 referencePosition) {
        return getCellsInRange(playerLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getProjectiles(Vector2 referencePosition) {
        return getCellsInRange(projectileLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getSpecialEffects(Vector2 referencePosition) {
        return getCellsInRange(specialEffectLayer, referencePosition);
    }


    private TiledMapTileLayer.Cell[] getCellsInRange(TiledMapTileLayer tileLayer, Vector2 referencePosition) {
        int xBeginning = (int) referencePosition.x - radius;
        int xEnding = (int) referencePosition.x + radius;

        int yBeginning = (int) referencePosition.y - radius;
        int yEnding = (int) referencePosition.y + radius;

        if (xBeginning < 0) {
            xBeginning = 0;
        }
        if (xEnding > floorLayer.getWidth() - 1) {
            xEnding = floorLayer.getWidth() - 1;
        }
        if (yBeginning < 0) {
            yBeginning = 0;
        }
        if (yEnding > floorLayer.getWidth() - 1) {
            yEnding = floorLayer.getHeight() - 1;
        }

        TiledMapTileLayer.Cell[] cells = new TiledMapTileLayer.Cell[(1 + xEnding - xBeginning) *
                (1 + yEnding - yBeginning)];

        int index = 0;

        for (int i = xBeginning; i < xEnding; i++) {
            for (int k = yBeginning; k < yEnding; k++) {
                TiledMapTileLayer.Cell cell = tileLayer.getCell(i, k);
                if (cell != null) {
                    cells[index] = cell;
                    index++;
                }
            }
        }

        return cells;
    }


    public void updateWalls(Enums.Direction direction) {
        switch (direction) {
            case NORTH:
                wallEastLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallNorthLayer.setVisible(true);
                break;
            case EAST:
                wallNorthLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallEastLayer.setVisible(true);
                break;
            case SOUTH:
                wallNorthLayer.setVisible(false);
                wallEastLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallSouthLayer.setVisible(true);
                break;
            case WEST:
                wallNorthLayer.setVisible(false);
                wallEastLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(true);
                break;
            default:
                break;
        }
    }


    public void dispose() {
        map.dispose();
    }
}
