package com.mygdx.game.Blessings;

import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Blessing extends Actor {

    public Blessing(GameController gameController) {
        this.gameController = gameController;

        type = Actor.ActorType.BUFF;
    }
}
