package com.mygdx.game.Traps;

import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Trap extends Actor {

    public Trap(GameController gameController) {
        this.gameController = gameController;

        type = ActorType.TRAP;
    }
}
