package com.mygdx.game.Controllers;

import com.mygdx.game.Characters.Player;
import com.mygdx.game.Levels.Level;
import com.mygdx.game.Levels.Level_Tutorial;
import com.mygdx.game.Screens.CreditsScreen;
import com.mygdx.game.Screens.HelpScreen;
import com.mygdx.game.Screens.InGameScreen;
import com.mygdx.game.Screens.LoadGameScreen;
import com.mygdx.game.Screens.MainMenuScreen;
import com.mygdx.game.Screens.NewGameScreen;
import com.mygdx.game.Screens.OptionsScreen;
import com.mygdx.game.Screens.SaveGameScreen;

/**
 * Created by Solist on 25/03/2017.
 */

public class GameController implements GameController_Interface {

    public MainMenuScreen mainMenuScreen;
    public NewGameScreen newGameScreen;
    public LoadGameScreen loadGameScreen;
    public SaveGameScreen saveGameScreen;
    public OptionsScreen optionsScreen;
    public HelpScreen helpScreen;
    public CreditsScreen creditsScreen;
    public InGameScreen inGameScreen;

    public float masterVolume = 1.0f;
    public float musicVolume = 1.0f;
    public float ambienceVolume = 1.0f;
    public float effectsVolume = 1.0f;
    public float dialogueVolume = 1.0f;

    public Boolean inGame = false;

    public Player player;

    public Level level;

    public CameraController inGameCamera;

    public void INIT() {
        inGameCamera = new CameraController();
        inGameCamera.setGameController(this);

        assetManager.setGameController(this);
        assetManager.INIT();

        mainMenuScreen = new MainMenuScreen(this);
        newGameScreen = new NewGameScreen(this);
        loadGameScreen = new LoadGameScreen(this);
        saveGameScreen = new SaveGameScreen(this);
        optionsScreen = new OptionsScreen(this);
        helpScreen = new HelpScreen(this);
        creditsScreen = new CreditsScreen(this);
        inGameScreen = new InGameScreen(this);

        guiController.setGameController(this);
    }

    public void newGame() {
        // Set each game object to reference this gameController.
        player = new Player(this);
        // Create the current level.
        level = new Level_Tutorial(this);
    }

    public void nextLevel(Level level) {
        // Set each game object to reference this gameController.
        player = new Player(this);
        // Create the current level.
    }

    public void loadGame(Level level, String stats) {
        // Set each game object to reference this gameController.
        player = new Player(this);
        // Create the current level.
    }

    public void dispose() {
        if (level != null) {
            level.dispose();
        }

        assetManager.dispose();
    }


    public void updateVolume() {
        updateMusicVolume();
        updateAmbienceVolume();
        updateEffectsVolume();
        updateDialogueVolume();
    }

    public void updateMusicVolume() {
        assetManager.menuMusic.setVolume(musicVolume * masterVolume);
    }

    public void updateAmbienceVolume() {

    }

    public void updateEffectsVolume() {

    }

    public void updateDialogueVolume() {

    }


    // Setters.

}
