package com.mygdx.game.Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 27/03/2017.
 */

public class AssetController extends GameObject {

    // General Variables.
    private InputMultiplexer multiplexer;
    public InputController inputController;
    public Stage stage;
    public FitViewport viewport;

    // Music Assets.
    public Music menuMusic;

    // Menu assets.
    public TextureAtlas menuAtlas;
    public Skin menuSkin;
    public SpriteBatch menuSprites;

    // In game UI assets.
    public TextureAtlas uiAtlas;
    public Skin uiSkin;
    public SpriteBatch uiSprites;

    // Background image.
    public Texture backgroundTexture;
    public SpriteBatch backgroundSprite;

    // In Game assets.
    public SpriteBatch playerSprites;
    public SpriteBatch floorSprites;
    public SpriteBatch wallSprites;
    public SpriteBatch[] trapSprites;
    public SpriteBatch[] blessingSprites;
    public SpriteBatch[] creatureSprites;


    public void INIT() {
        multiplexer = new InputMultiplexer();
        inputController = new InputController();
        inputController.setGameController(gameController);

        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                gameController.uiCamera);
        viewport.apply();

        gameController.uiCamera.position.set(Gdx.graphics.getWidth() / 2,
                Gdx.graphics.getHeight() / 2, 0);
        gameController.uiCamera.update();

        gameController.uiCamera.position.set(Gdx.graphics.getWidth() / 2,
                Gdx.graphics.getHeight() / 2, 0);
        gameController.uiCamera.update();

        // Set the background texture.
        backgroundTexture = new Texture(Gdx.files.internal("Shield.png"));
        backgroundSprite = new SpriteBatch();

        // Set the menu assets.
        menuAtlas = new TextureAtlas("menu/menu-atlas.txt");
        menuSkin = new Skin(Gdx.files.internal("menu/menu-skin.json"), menuAtlas);
        menuSprites = new SpriteBatch();

        // Set the in game UI assets.
        uiAtlas = new TextureAtlas("ui/ui-atlas.txt");
        uiSkin = new Skin(Gdx.files.internal("ui/ui-skin.json"), uiAtlas);
        uiSprites = new SpriteBatch();

        // Set the in game assets.
        SpriteBatch playerSprites = new SpriteBatch(); // need to set this.
        SpriteBatch floorSprites = new SpriteBatch();
        SpriteBatch wallSprites = new SpriteBatch();
        SpriteBatch[] trapSprites = new SpriteBatch[0];
        SpriteBatch[] blessingSprites = new SpriteBatch[0];
        SpriteBatch[] creatureSprites = new SpriteBatch[0];

        multiplexer.addProcessor(inputController);
        multiplexer.addProcessor(new Stage(viewport, menuSprites));
        Gdx.input.setInputProcessor(multiplexer);
    }


    public void setStage(SpriteBatch sprites) {
        stage = new Stage(viewport, sprites);

        // The Stage should control the input.
        multiplexer.removeProcessor(multiplexer.size() - 1);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }


    public void dispose() {

    }
}
