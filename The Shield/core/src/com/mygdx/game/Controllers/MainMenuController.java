package com.mygdx.game.Controllers;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public class MainMenuController extends GameObject {

    // Variables.
    public Button newGame_Button = new Button();
    public Button loadGame_Button = new Button();
    public Button options_Button = new Button();
    public Button help_Button = new Button();
    public Button credits_Button = new Button();
    public Button exit_Button = new Button();


    // Methods.

    // Button Clicks.
    public void NewGame() {

    }

    public void LoadGame() {

    }

    public void Options() {

    }

    public void Help() {

    }

    public void Credits() {

    }

    public void Exit() {

    }


    // Setters.
    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }
}
