package com.mygdx.game.Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public class CameraController extends GameObject {

    // Variables.
    public OrthographicCamera camera;

    private float startTime = 0;
    private float currentTime = 0;

    public Vector2 Position = new Vector2();
    public Enums.Direction Direction = Enums.Direction.NORTH;
    private int rotation;


    // Methods.

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the movement methods.
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Movement.
    public void MoveForward(float delta) {
        // Smoothly move the camera forward 1 unit (square).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.movementSpeed) {
            switch (Direction) {
                case NORTH:
                    // Move along positive Y axis.
                    // Move character.
                    gameController.player.MoveForward();
                    break;
                case EAST:
                    // Move along positive X axis.
                    // Move character.
                    gameController.player.MoveForward();
                    break;
                case SOUTH:
                    // Move along negative Y axis.
                    // Move character.
                    gameController.player.MoveForward();
                    break;
                case WEST:
                    // Move along negative X axis.
                    // Move character.
                    gameController.player.MoveForward();
                    break;
                default:
                    break;
            }
        }
        else {
            gameController.player.isMoving = false;
            // fixPosition() method is used to update the position to the correct position as the
            // movement over time may not put us at the desired location.
            fixPosition();
        }
        // and move the character at the same time.
        gameController.player.MoveForward();
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the rotation methods.
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Rotation.
    public void TurnLeft(float delta) {
        // Smoothly rotate the camera 90 degrees to the left (-90 degrees).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.rotationSpeed) {
            /*int tempRotation = (int)-((gameController.player.rotationSpeed / 90) *
                    (currentTime - startTime));
            camera.rotate(tempRotation);
            rotation += tempRotation;*/
        }
        else {
            gameController.player.isRotatingLeft = false;
            gameController.level.updateWalls(Direction);
            fixCameraRotation();
            gameController.inGameScreen.populateUITable();
        }
        // and rotate the character at the same time.

        // Change the camera's direction
        // (i.e. if the character's current direction is NORTH, change their Direction to WEST).
    }

    public void TurnRight(float delta) {
        // Smoothly rotate the camera 90 degrees to the right (90 degrees).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.rotationSpeed) {
            /*
            int tempRotation = (int)((gameController.player.rotationSpeed / 90) *
                    (currentTime - startTime));
            camera.rotate(tempRotation);
            rotation += tempRotation;*/
        }
        else {
            gameController.player.isRotatingRight = false;
            gameController.level.updateWalls(Direction);
            fixCameraRotation();
            gameController.inGameScreen.populateUITable();
        }
        // and rotate the character at the same time.

        // Change the camera's direction
        // (i.e. if the character's current direction is NORTH, change their Direction to EAST).
    }


    private void fixPosition() {

    }


    private void fixCameraRotation() {
        if (rotation < 0) {
            rotation = 360 + rotation;
        }
        else if (rotation > 360) {
            rotation = 360;
        }

        int tempRotation = 0;
        switch (Direction) {
            case NORTH:
                /*if (rotation >= 270) {
                    tempRotation = 360 - rotation;
                }
                else {
                    tempRotation = rotation;
                }*/
                camera.rotate(0 - rotation);
                rotation = 0;
                break;
            case EAST:
                camera.rotate(90 - rotation);
                rotation = 90;
                break;
            case SOUTH:
                camera.rotate(180 - rotation);
                rotation = 180;
                break;
            case WEST:
                camera.rotate(270 - rotation);
                rotation = 270;
                break;
            default:
                break;
        }
    }


    // Setters.
    public void setStartTime(float startTime) {
        this.startTime = startTime;
        currentTime = startTime;
    }
}
