package com.mygdx.game.Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

// For information regarding accelerometers
// go here https://github.com/libgdx/libgdx/wiki/Accelerometer
// (Referenced 25/03/2017)

public class InputController extends GameObject implements InputProcessor {

    int orientation = Gdx.input.getRotation();


    public void CheckInputs() {
        switch (Gdx.app.getType()) {
            case Android:
                CheckAndroidInputs();
                break;
            case iOS:
                CheckIOSInputs();
                break;
            default:
                break;
        }
    }


    // !!!!!!!!!!!!!!!!!!!!!
    // TODO - Get the accelerometer inputs for android devices.
    // !!!!!!!!!!!!!!!!!!!!!
    private void CheckAndroidInputs() {
        /*if (Gdx.input.getAccelerometerX() > 0) {
            gameController.inGameCamera.MoveForward();
        } else if (Gdx.input.getAccelerometerY() > 0) {
            gameController.inGameCamera.TurnLeft();
        } else if (Gdx.input.getAccelerometerZ() > 0) {
            gameController.inGameCamera.TurnRight();
        }*/
        // else if (attack button pressed) { }
        // else if (block button pressed) { }
        // else if (weaponSwap button pressed) { }
        // else if (menu button pressed) { }
    }

    private void CheckIOSInputs() {
        /*
        if (Gdx.input.getAccelerometerX() > 0) {
            gameController.inGameCamera.MoveForward();
        } else if (Gdx.input.getAccelerometerY() > 0) {
            gameController.inGameCamera.TurnLeft();
        } else if (Gdx.input.getAccelerometerZ() > 0) {
            gameController.inGameCamera.TurnRight();
        }*/
        // else if (attack button pressed) { }
        // else if (block button pressed) { }
        // else if (weaponSwap button pressed) { }
        // else if (menu button pressed) { }
    }


    public boolean keyDown (int keycode) {
        if (gameController.inGame == true && gameController.player.performingActions() == false) {
            if (keycode == Input.Keys.W) {
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isMoving = true;
            } else if (keycode == Input.Keys.A) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        gameController.player.Direction = Enums.Direction.WEST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        gameController.player.Direction = Enums.Direction.NORTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        gameController.player.Direction = Enums.Direction.EAST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        gameController.player.Direction = Enums.Direction.SOUTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingLeft = true;
            } else if (keycode == Input.Keys.D) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        gameController.player.Direction = Enums.Direction.EAST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        gameController.player.Direction = Enums.Direction.SOUTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        gameController.player.Direction = Enums.Direction.WEST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        gameController.player.Direction = Enums.Direction.NORTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingRight = true;
            }
            return true;
        }
        return false;
    }

    public boolean keyUp (int keycode) {
        return false;
    }

    public boolean keyTyped (char character) {
        return false;
    }

    public boolean touchDown (int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        return false;
    }

    public boolean mouseMoved (int x, int y) {
        return false;
    }

    public boolean scrolled (int amount) {
        return false;
    }
}
