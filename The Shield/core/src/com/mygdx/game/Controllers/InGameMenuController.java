package com.mygdx.game.Controllers;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public class InGameMenuController extends GameObject {

    // Variables.
    public Button resume_Button = new Button();
    public Button saveGame_Button = new Button();
    public Button loadGame_Button = new Button();
    public Button options_Button = new Button();
    public Button help_Button = new Button();
    public Button exit_Button = new Button();


    public void Resume() {

    }

    public void SaveGame() {

    }

    public void LoadGame() {

    }

    public void Options() {

    }

    public void Help() {

    }

    public void Exit() {

    }
}
