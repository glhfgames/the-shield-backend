package com.mygdx.game.Controllers;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public class GUIController extends GameObject {

    // Variables.
    public Button menu_Button = new Button();
    public Button attack_Button = new Button();
    public Button block_Button = new Button();
    public Button weaponSwap_Button = new Button();


    // Menu Methods.
    public void MenuButton_Click() {

    }


    // Weapon Buttons Methods.
    public void AttackButton_Click() {

    }

    public void BlockButton_Click() {

    }

    public void WeaponSwapButton_Click() {

    }


    // Compass Methods.
    public void ChangeCompass() {

    }
}
