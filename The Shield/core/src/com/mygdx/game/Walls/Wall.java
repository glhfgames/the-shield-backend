package com.mygdx.game.Walls;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.Enums;

/**
 * Created by Solist on 25/03/2017.
 */

public class Wall extends Actor {
    public WallType wallType;

    public enum WallType {
        SINGLE,
        MIDDLE,
        LEFT,
        RIGHT,
        LEFTCAP,
        RIGHTCAP,
        BOTTOMLEFTBEND,
        BOTTOMRIGHTBEND,
        TOPLEFTBEND,
        TOPRIGHTBEND,
        LEFTCORNER,
        RIGHTCORNER,
        DOORFRAMELEFT,
        DOORFRAMERIGHT,
        DOORFRAMETOP,
        DOORFRAMEBOTTOM,
    }


    public Wall(Texture tex, WallType wallType, Vector2 position, GameController gameController) {
        this.gameController = gameController;

        type = ActorType.WALL;

        Position = position;

        Direction = Enums.Direction.NORTH;

        this.wallType = wallType;

        switch (wallType) {
            case BOTTOMLEFTBEND:
                currentSpriteIndex = 0;
                break;
            case BOTTOMRIGHTBEND:
                currentSpriteIndex = 1;
                break;
            case TOPLEFTBEND:
                currentSpriteIndex = 2;
                break;
            case TOPRIGHTBEND:
                currentSpriteIndex = 3;
                break;
            case LEFTCAP:
                currentSpriteIndex = 4;
                break;
            case RIGHTCAP:
                currentSpriteIndex = 5;
                break;
            case LEFTCORNER:
                currentSpriteIndex = 6;
                break;
            case RIGHTCORNER:
                currentSpriteIndex = 7;
                break;
            case LEFT:
                currentSpriteIndex = 8;
                break;
            case MIDDLE:
                currentSpriteIndex = 9;
                break;
            case RIGHT:
                currentSpriteIndex = 10;
                break;
            case SINGLE:
                currentSpriteIndex = 11;
                break;
            case DOORFRAMELEFT:
                currentSpriteIndex = 12;
                break;
            case DOORFRAMERIGHT:
                currentSpriteIndex = 13;
                break;
            case DOORFRAMETOP:
                currentSpriteIndex = 14;
                break;
            case DOORFRAMEBOTTOM:
                currentSpriteIndex = 15;
                break;
            default:
                break;
        }


        int amountOfTextures = 12;
        int amountOfColumns = 12;
        int amountOfRows = 1;
        boolean limitReached = false;
        sprites = new TextureRegion[amountOfTextures];
        for (int i = 0; i < amountOfRows; i++) {
            for (int k = 0; k < amountOfColumns; k++) {
                if (i + (amountOfRows * k) > amountOfTextures - 1) {
                    limitReached = true;
                    break;
                }
                sprites[i + (amountOfRows * k)] = new TextureRegion(tex, 128 * k, 0, 128, 128);
            }
            if (limitReached == true) {
                break;
            }
        }
    }
}
