package com.mygdx.game.Floors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.Enums;

/**
 * Created by Solist on 25/03/2017.
 */

public class Floor extends Actor {
    public FloorType floorType;

    public enum FloorType {
        TOPLEFT,
        TOP,
        TOPRIGHT,
        LEFT,
        MIDDLE,
        RIGHT,
        BOTTOMLEFT,
        BOTTOM,
        BOTTOMRIGHT,
        TOPLEFTCORNER,
        TOPRIGHTCORNER,
        BOTTOMLEFTCORNER,
        BOTTOMRIGHTCORNER,
        TOPBOTTOMLEFTCORNER,
        TOPBOTTOMRIGHTCORNER,
        TOPLEFTRIGHTCORNER,
        BOTTOMLEFTRIGHTCORNER,
        TOPBOTTOM,
        LEFTRIGHT,
        DOORSIDE,
        DOORFRONT,
    }


    public Floor(Texture tex, FloorType floorType, Vector2 position, GameController gameController) {
        this.gameController = gameController;

        type = ActorType.FLOOR;

        Position = position;

        Direction = Enums.Direction.NORTH;

        this.floorType = floorType;

        switch (floorType) {
            case MIDDLE:
                currentSpriteIndex = 0;
                break;
            case BOTTOM:
                currentSpriteIndex = 1;
                break;
            case BOTTOMLEFT:
                currentSpriteIndex = 2;
                break;
            case BOTTOMRIGHT:
                currentSpriteIndex = 3;
                break;
            case BOTTOMLEFTCORNER:
                currentSpriteIndex = 4;
                break;
            case BOTTOMLEFTRIGHTCORNER:
                currentSpriteIndex = 5;
                break;
            case BOTTOMRIGHTCORNER:
                currentSpriteIndex = 6;
                break;
            case TOPBOTTOMLEFTCORNER:
                currentSpriteIndex = 7;
                break;
            case TOPBOTTOMRIGHTCORNER:
                currentSpriteIndex = 8;
                break;
            case TOPLEFTCORNER:
                currentSpriteIndex = 9;
                break;
            case TOPLEFTRIGHTCORNER:
                currentSpriteIndex = 10;
                break;
            case TOPRIGHTCORNER:
                currentSpriteIndex = 11;
                break;
            case LEFT:
                currentSpriteIndex = 12;
                break;
            case LEFTRIGHT:
                currentSpriteIndex = 13;
                break;
            case RIGHT:
                currentSpriteIndex = 14;
                break;
            case TOP:
                currentSpriteIndex = 15;
                break;
            case TOPBOTTOM:
                currentSpriteIndex = 16;
                break;
            case TOPLEFT:
                currentSpriteIndex = 17;
                break;
            case TOPRIGHT:
                currentSpriteIndex = 18;
                break;
            case DOORSIDE:
                currentSpriteIndex = 19;
                break;
            case DOORFRONT:
                currentSpriteIndex = 20;
                break;
            default:
                break;
        }


        int amountOfTextures = 19;
        int amountOfColumns = 2;
        int amountOfRows = 10;
        boolean limitReached = false;
        sprites = new TextureRegion[amountOfTextures];
        for (int i = 0; i < amountOfRows; i++) {
            for (int k = 0; k < amountOfColumns; k++) {
                if (i + (amountOfRows * k) > amountOfTextures - 1) {
                    limitReached = true;
                    break;
                }
                sprites[i + (amountOfRows * k)] = new TextureRegion(tex, 128 * i, 0, 128, 128);
            }
            if (limitReached == true) {
                break;
            }
        }
    }


    public void render(SpriteBatch batch) {
        batch.draw(sprites[currentSpriteIndex], Position.x, Position.y);
    }
}
