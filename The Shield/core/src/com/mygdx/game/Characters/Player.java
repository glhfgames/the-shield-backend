package com.mygdx.game.Characters;

import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.PlayingActor;

/**
 * Created by Solist on 26/03/2017.
 */

public class Player extends PlayingActor {


    public Player(GameController gameController) {
        this.gameController = gameController;

        type = ActorType.PLAYER;
    }

    public void render() {

    }
}
