package com.mygdx.game.Characters;

import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.PlayingActor;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Creature extends PlayingActor {


    public Creature(GameController gameController) {
        this.gameController = gameController;

        type = ActorType.ENEMY;
    }
}
